import Express, { Application, Request, Response } from "express";
import morgan = require("morgan");
import { component, initialize, inject } from "tsdi";
import { BotConfig } from "./config";
import { Images } from "./images";
import { Logger } from "./logger";
import { Search } from "./search";

@component
export class HttpServer {
    @inject private config!: BotConfig;
    @inject private logger!: Logger;
    @inject private search!: Search;
    @inject private images!: Images;

    private app: Application;

    @initialize protected initialize(): void {
        this.app = Express();
        this.app.use(morgan("tiny", { stream: { write: (msg) => this.logger.info(msg.trim()) } }));
        this.app.get("/:ref", (req, res) => this.onGetRef(req, res));
        this.logger.info(`HTTP server listening on port ${this.config.port}`);
        this.app.listen(this.config.port);
    }

    private async onGetRef(req: Request, res: Response): Promise<void> {
        const searchResult = this.search.get(req.params.ref.replace(".jpg", ""));
        if (!searchResult) {
            res.status(400).send();
            return;
        }
        const buffer = await this.images.getMeme(searchResult);
        res.status(200).contentType("image/jpeg").send(buffer);
    }
}
