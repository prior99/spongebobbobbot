import { component, inject } from "tsdi";
import fetch from "node-fetch";
import { exec } from "child_process";
import * as promises from "fs/promises";
import { BotConfig } from "./config";
import { Logger } from "./logger";
import { existsSync } from "fs";
import * as path from "path";
import { Canvas, Image, loadImage, registerFont } from "canvas";
import { SearchResult } from "./search";
import { dirname } from "path";

const fontSize = 35;
const padding = 5;

registerFont(path.join(__dirname, "..", "fonts", "anton", "anton-regular.ttf"), { family: "Anton" });

@component
export class Images {
    @inject private readonly config!: BotConfig;
    @inject private readonly logger!: Logger;

    private async getImage(ref: string): Promise<Image> {
        if (!existsSync(this.config.cache)) {
            this.logger.error(`Cache directory ${this.config.cache} does not exist.`);
        }
        const [_string, season, episode, frame] = /s(\d+)e(\d+)_(\d+)/.exec(ref);
        const url = `https://subsearch.app/static/data/thumbs/season-${season}/episode-${episode}/s${season}e${episode}_${frame}.bpg`;
        const request = await fetch(url);
        const body = await request.buffer();
        const bpgFileName = path.join(this.config.cache, `${ref}.bpg`);
        const pngFileName = path.join(this.config.cache, `${ref}.png`);
        await promises.writeFile(bpgFileName, body);
        await new Promise<void>((resolve, reject) => {
            exec(`bpgdec "${bpgFileName}" -o ${pngFileName}`, (err) => {
                if (err) {
                    reject(err);
                }
                resolve();
            });
        });
        return await loadImage(pngFileName);
    }

    private getLines(ctx: CanvasRenderingContext2D, text: string, maxWidth: number): string[] {
        const cleaned = text.replace(/♪/g, "").replace(/<\/?[ib]>/g, "");
        const words = cleaned.split(/\s+/);
        const lines: string[] = [];
        let currentLine = words[0];

        for (let i = 1; i < words.length; i++) {
            let word = words[i];
            let width = ctx.measureText(`${currentLine} ${word}`).width;
            if (width < maxWidth) {
                currentLine += ` ${word}`;
            } else {
                lines.push(currentLine);
                currentLine = word;
            }
        }
        lines.push(currentLine);
        return lines;
    }

    public async getMeme(result: SearchResult): Promise<Buffer> {
        const memeFileName = path.join(this.config.cache, `${result.index}-meme.png`);
        if (existsSync(memeFileName)) {
            return await promises.readFile(memeFileName);
        }
        const image = await this.getImage(result.index);
        const canvas = new Canvas(image.width, image.height);
        const context = await canvas.getContext("2d");
        context.drawImage(image, 0, 0);
        context.textAlign = "center";
        context.fillStyle = "white";
        context.strokeStyle = "black";
        context.lineWidth = 2;
        context.font = `${fontSize}px Anton`;
        let lineNumber = 0;
        for (const line of this.getLines(context, result.content, image.width - 2 * padding)) {
            const y = (fontSize + padding) * (lineNumber + 1);
            const x = image.width / 2;
            context.fillText(line, x, y);
            context.strokeText(line, x, y);
            lineNumber++;
        }
        const buffer = canvas.toBuffer();
        await promises.writeFile(memeFileName, buffer);
        return buffer;
    }
}
