import { Component } from "tsdi";
import * as Winston from "winston";

export function createLogger(): Winston.Logger {
  return Winston.createLogger({
    transports: [
      new Winston.transports.Console({
        format: Winston.format.combine(
          Winston.format.timestamp({ format: "YYYY-MM-DDTHH:mm:ss" }),
          Winston.format.colorize(),
          Winston.format.printf(
            (info) => `${info.timestamp} - ${info.level}: ${info.message}`
          )
        ),
        level: "verbose",
      }),
    ],
  });
}

@Component
export class Logger {
  private logger = createLogger();

  public get debug(): Winston.Logger["debug"] {
    return this.logger.debug.bind(this.logger);
  }

  public get error(): Winston.Logger["error"] {
    return this.logger.error.bind(this.logger);
  }

  public get warn(): Winston.Logger["warn"] {
    return this.logger.warn.bind(this.logger);
  }

  public get info(): Winston.Logger["info"] {
    return this.logger.info.bind(this.logger);
  }

  public get verbose(): Winston.Logger["verbose"] {
    return this.logger.verbose.bind(this.logger);
  }
}
