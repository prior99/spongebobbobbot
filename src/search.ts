import { component, initialize, inject } from "tsdi";
import fetch from "node-fetch";
import * as promises from "fs/promises";
import { BotConfig } from "./config";
import { Logger } from "./logger";
import { existsSync } from "fs";
import * as YAML from "yamljs";
import { Index, SerialisedIndexData } from "elasticlunr";

const searchIndexUrl = "https://subsearch.app/static/data/search-index.json";
const timingsUrl = "https://subsearch.app/static/data/combined-timings.json";

export interface Timing {
    end: string;
    start: string;
}

export interface Timings {
    [key: string]: Timings;
}

export interface SearchResult {
    content: string;
    index: string;
}

@component
export class Search {
    @inject private readonly logger!: Logger;
    @inject private readonly config!: BotConfig;

    private index: Index<SearchResult>;
    private timings: Timings;

    @initialize
    protected async initialize(): Promise<void> {
        this.logger.info("Search ready.");
        const index = await this.getSearchIndex();
        try {
            this.index = Index.load(index);
        } catch (err) {
            this.logger.error(`Failed to initialize search index: ${err.message}`);
        }
        this.timings = await this.getTimings();
    }

    public get(ref: string): SearchResult | undefined {
        const doc = this.index.documentStore.getDoc(ref);
        if (!doc) {
            return;
        }
        return {
            ...doc,
            content: doc.content.replace(/♪/g, "").replace(/<\/?[ib]>/g, "")
        }
    }

    public search(search: string): SearchResult[] {
        return this.index.search(search, { expand: true }).map(({ ref }) => this.get(ref));
    }

    private async getSearchIndex(): Promise<SerialisedIndexData<SearchResult>> {
        if (existsSync(this.config.indexFile)) {
            this.logger.info(`Found index file: ${this.config.indexFile}`);
            try {
                const index = await YAML.load(this.config.indexFile);
                this.logger.info("Index loaded from file.");
                return index;
            } catch (err) {
                this.logger.error(`Failed to load index file. Re-downloading.`);
                return await this.fetchSearchIndex();
            }
        } else {
            return await this.fetchSearchIndex();
        }
    }

    private async getTimings(): Promise<Timings> {
        if (existsSync(this.config.timingsFile)) {
            this.logger.info(`Found timings file: ${this.config.timingsFile}`);
            try {
                const timings = await YAML.load(this.config.timingsFile);
                this.logger.info("Timings loaded from file.");
                return timings;
            } catch (err) {
                this.logger.error(`Failed to load timings file. Re-downloading.`);
                return await this.fetchTimings();
            }
        } else {
            return await this.fetchTimings();
        }
    }

    private async fetchSearchIndex(): Promise<SerialisedIndexData<SearchResult>> {
        this.logger.info("Downloading search index...");
        const request = await fetch(searchIndexUrl);
        if (!request.ok) {
            this.logger.error(`Failed to fetch search index. Invalid status code: ${request.status}`);
        }
        try {
            const searchIndex = await request.json();
            this.logger.info(`Saving search index to ${this.config.indexFile}.`);
            promises.writeFile(this.config.indexFile, YAML.stringify(searchIndex), "utf8");
            return searchIndex;
        } catch (err) {
            this.logger.error("Failed to parse search index.");
            throw err;
        }
    }

    private async fetchTimings(): Promise<Timings> {
        this.logger.info("Downloading timings...");
        const request = await fetch(timingsUrl);
        if (!request.ok) {
            this.logger.error(`Failed to fetch search index. Invalid status code: ${request.status}`);
        }
        try {
            const timings = await request.json();
            this.logger.info(`Saving timings to ${this.config.timingsFile}.`);
            promises.writeFile(this.config.timingsFile, YAML.stringify(timings), "utf8");
            return timings;
        } catch (err) {
            this.logger.error("Failed to parse timings.");
            throw err;
        }
    }
}
