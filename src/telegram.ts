import { InlineQuery, InlineQueryResultPhoto } from "node-telegram-bot-api";
import TelegramBot from "node-telegram-bot-api";
import { component, initialize, inject } from "tsdi";
import { BotConfig } from "./config";
import { Images } from "./images";
import { Logger } from "./logger";
import { Search } from "./search";

@component
export class Telegram {
    @inject private readonly logger!: Logger;
    @inject private readonly config!: BotConfig;
    @inject private readonly search!: Search;
    @inject private readonly images!: Images;

    private telegramBot!: TelegramBot;

    @initialize protected async initialize(): Promise<void> {
        this.logger.info("Connecting to telegram.");
        this.telegramBot = new TelegramBot(this.config.telegramBotToken, { polling: true });
        this.telegramBot.on("inline_query", async (query: InlineQuery) => this.onInlineQuery(query));
        this.telegramBot.on("error", (err) => this.logger.error(`Telegram bot: ${err.message}`, err));
        this.telegramBot.on("polling_error", (err) => this.logger.error(`Telegram bot polling: ${err.message}`, err));
    }

    private async onInlineQuery(query: InlineQuery): Promise<void> {
        const all = this.search.search(query.query);
        const offset = query.offset ? Number(query.offset) : 0;
        const searchResults = all.slice(offset, offset + 5);
        this.logger.info(`Query "${query.query}" yielded ${all.length} results.`);
        const results: InlineQueryResultPhoto[] = await Promise.all(
            searchResults.map(
                async (searchResult): Promise<InlineQueryResultPhoto> => {
                    await this.images.getMeme(searchResult);
                    const url = `${this.config.publicUrl}/${searchResult.index}.jpg`;
                    return {
                        type: "photo",
                        id: searchResult.index,
                        thumb_url: url,
                        photo_url: url,
                        photo_width: 480,
                        photo_height: 360,
                        title: searchResult.content,
                        caption: searchResult.content,
                    };
                },
            ),
        );
        await this.telegramBot.answerInlineQuery(query.id, results, { cache_time: 60, next_offset: `${offset + 5}` });
    }
}
