import { Command, command, metadata } from "clime";
import { TSDI } from "tsdi";
import { BotConfig } from "../config";
import { handlePromises } from "../handle-promises";
import { onKill } from "../kill";
import { SpongebobBobBot } from "../spongebobbobbot";
import { Logger } from "../logger";
import { Search } from "../search";
import { Images } from "../images";
import { Telegram } from "../telegram";
import { HttpServer } from "../http-server";

@command({ description: "Start the bot." })
export default class StartCommand extends Command {
    @metadata
    public async execute(config: BotConfig): Promise<void> {
        const tsdi = new TSDI(new SpongebobBobBot(config));
        await tsdi.asyncGet(Telegram);
        await tsdi.asyncGet(HttpServer);
        onKill(() => tsdi.close(), tsdi.get(Logger));
        handlePromises(tsdi.get(Logger));
    }
}
