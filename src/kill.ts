import { Logger } from "./logger";

export function onKill(callback: () => void, logger: Logger): void {
    let killed = false;
    const kill = async (): Promise<void> => {
        if (killed) {
            logger.error("CTRL^C detected. Terminating!");
            // eslint-disable-next-line no-process-exit
            process.exit(1);
        }
        killed = true;
        logger.warn("CTRL^C detected. Secure shutdown initiated.");
        logger.warn("Press CTRL^C again to terminate at your own risk.");
        callback();
    };
    process.on("SIGINT", kill);
}
