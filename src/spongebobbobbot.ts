import { configure } from "tsdi";
import { BotConfig } from "./config";
import { HttpServer } from "./http-server";
import { Images } from "./images";
import { Logger } from "./logger";
import { Search } from "./search";
import { Telegram } from "./telegram";

export class SpongebobBobBot {
    @configure protected readonly logger!: Logger;
    @configure protected readonly search!: Search;
    @configure protected readonly images!: Images;
    @configure protected readonly telegram!: Telegram;
    @configure protected readonly httpServer!: HttpServer;

    @configure
    protected botConfig(): BotConfig {
        return this.config;
    }

    constructor(private config: BotConfig) {}
}
